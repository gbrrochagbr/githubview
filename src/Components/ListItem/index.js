import React from "react";
import { withStyles } from "@material-ui/styles";
import Octicon, { Star } from "@githubprimer/octicons-react";

const styles = {
  listItem: {
    border: "1px solid #797979",
    borderWidth: "1px 0 1px",
    "&:not(:last-of-type)": {
      borderBottom: 0
    },
    boxSizing: "border-box",
    padding: "6px 10px",
    color: "#797979",
    cursor: "pointer",
    userSelect: "none",
    transition: "background .2s",
    "&:hover": {
      background: "rgba(0,0,0,.3)",
      transition: "background .3s"
    }
  },
  itemHeader: {
    display: "flex",
    justifyContent: "flex-end"
  },
  starIcon: {
    marginLeft: 10,
    "& > path": {
      fill: "goldenrod"
    }
  },
  repoName: {
    fontSize: 20,
    margin: "4px 0"
  }
};

export default withStyles(styles)(
  class ListItem extends React.Component {
    render() {
      const { name, stars, classes, onClick } = this.props;

      return (
        <div className={classes.listItem} onClick={onClick}>
          <div className={classes.itemHeader}>
            {stars}
            <Octicon
              className={classes.starIcon}
              icon={Star}
              ariaLabel="Stars"
            />
          </div>
          <div className={classes.repoName}>{name}</div>
        </div>
      );
    }
  }
);
