import React from "react";
import { withStyles } from "@material-ui/styles";

var styles = () => ({
  modal: {
    padding: 20,
    position: "fixed",
    background: "#fff",
    width: "calc(100% - 40px)",
    boxSizing: "border-box",
    borderRadius: 6,
    boxShadow: "3px 2px 7px rgba(0,0,0,.7)",
    zIndex: 2,
    maxWidth: 500
  },
  boxModal: {
    width: "100vw",
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    top: 0
  },
  boxMessage: {
    marginBottom: 18,
    wordBreak: "break-word",
    textAlign: "center",
    lineHeight: "26px"
  },
  boxAction: {
    textAlign: "right"
  },
  button: {
    padding: "8px 20px",
    borderRadius: 4,
    background: "none",
    border: "0.5px solid #efefef",
    outline: "none",
    "&:active": {
      background: "rgba(0,0,0,.2)"
    }
  },
  backdrop: {
    position: "fixed",
    zIndex: 1,
    top: 0,
    width: "100vw",
    height: "100%",
    background: "#000",
    opacity: 0.5
  }
});

export default withStyles(styles)(
  class ModalMessage extends React.Component {
    render() {
      const {
        classes,
        action,
        actionText = "OK",
        backdropAction,
        message = "",
        ...props
      } = this.props;

      return (
        <React.Fragment>
          <div
            className={classes.backdrop}
            onClick={backdropAction && backdropAction}
          />
          <div className={classes.boxModal}>
            <div className={classes.modal} {...props}>
              <div className={classes.boxMessage}>{message}</div>
              <div className={classes.boxAction}>
                <button className={classes.button} onClick={action && action}>
                  {actionText}
                </button>
              </div>
            </div>
          </div>
        </React.Fragment>
      );
    }
  }
);
