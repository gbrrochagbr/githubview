import React from "react";
import { withStyles } from "@material-ui/styles";
import ModalMessage from "../../Components/ModalMessage";
import { Link } from "react-router-dom";

const styles = () => ({
  container: {
    background: "linear-gradient(#222, #121212)",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
    minHeight: "100vh"
  },
  title: {
    textAlign: "center",
    color: "#fff",
    fontSize: "1.8rem"
  },
  or: {
    textAlign: "center",
    marginBottom: 20,
    color: "#fff"
  },
  optionsWrapper: {
    width: "80%",
    maxWidth: 400,
    padding: 16,
    display: "flex",
    flexDirection: "column"
  },
  input: {
    borderRadius: 4,
    width: "100%",
    height: 50,
    marginBottom: 16,
    boxSizing: "border-box",
    padding: 10,
    fontSize: "1.3rem",
    border: "none",
    background: "#fff",
    outline: "none",
    "&::placeholder": {
      color: "#999",
      fontWeight: 400,
      fontSize: "1rem"
    }
  },
  userLink: {
    borderRadius: 4,
    width: "100%",
    height: 50,
    marginBottom: 16,
    boxSizing: "border-box",
    padding: 10,
    fontSize: "1.3rem",
    border: "none",
    background: "#fff",
    outline: "none",
    position: "relative",
    display: "flex",
    alignItems: "center",
    textDecoration: "none",
    color: "#222",
    transition: "background .2s",

    "&:hover": {
      background: "#ddd",
      transition: "background .2s"
    },

    "&::after": {
      content: `""`,
      display: "inline-block",
      height: 20,
      width: 20,
      border: "4px solid #333",
      borderWidth: "4px 4px 0 0",
      borderRadius: 4,
      position: "absolute",
      transform: "rotate(45deg)",
      right: "20px"
    }
  },
  loginBottom: {
    display: "flex",
    justifyContent: "space-between",
    height: 40
  },
  littleLink: {
    width: 66,
    borderRadius: 4,
    border: "none",
    background: "#fff",
    outline: "none"
  },
  btnLink: {
    background: "none",
    border: "none",
    display: "inline-flex",
    alignItems: "center",
    padding: 8,
    height: "auto",
    width: "auto",
    textDecoration: "underline",
    fontWeight: 500,
    cursor: "pointer",
    outline: "none",
    fontSize: ".82rem",
    color: "#eee"
  },
  wrapperInput: {
    display: "flex"
  },
  userInput: {
    margin: 0,
    marginRight: 20
  }
});

export default withStyles(styles)(
  class Home extends React.Component {
    state = { user: "", openModal: false };

    handleClick = e => {
      const { user } = this.state;

      if (!user) {
        this.toggleModal("open");
        e.preventDefault();
      }
    };

    toggleModal = act => {
      if (act === "open") {
        this.setState({ openModal: true });
      } else {
        this.setState({ openModal: false });
      }
    };

    handleChange = e => {
      this.setState({ [e.target.name]: e.target.value });
    };

    render() {
      const { classes } = this.props;
      const { user, openModal } = this.state;

      return (
        <div className={classes.container}>
          <div className={classes.optionsWrapper}>
            <h2 className={classes.title}>Selecione um usuário</h2>
            <Link to="facebook" className={classes.userLink}>
              facebook
            </Link>
            <div className={classes.or}>Ou</div>
            <div className={classes.wrapperInput}>
              <input
                className={`${classes.input} ${classes.userInput}`}
                name="user"
                value={user}
                onChange={this.handleChange}
                placeholder="Usuário..."
              />
              <Link
                className={`${classes.userLink} ${classes.littleLink}`}
                onClick={this.handleClick}
                to={user}
              />
            </div>

            {/*             <div className={classes.loginBottom}>

              <Link to="/signUp" className={classes.btnLink}>
                {" "}
                Cadastre-se
              </Link>
            </div>
            */}
          </div>
          {openModal && (
            <ModalMessage
              message="Digite um usuário ou selecione o pré definido para prosseguir :)"
              action={() => {
                this.toggleModal("close");
              }}
              backdropAction={() => {
                this.toggleModal("close");
              }}
            />
          )}
        </div>
      );
    }
  }
);
