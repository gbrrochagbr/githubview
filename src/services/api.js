import axios from "axios";

const api = axios.create({
  baseURL: "https://api.github.com"
  //https://api.github.com/search/repositories?q=user:facebook&sort=stars&type=Repositories&order=desc
});

export default api;
