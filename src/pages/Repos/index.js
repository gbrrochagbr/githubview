import React from "react";
/* import api from "../../services/api"; */
import { withStyles } from "@material-ui/styles";
import RepoList from "../../Components/RepoList";
/* import repoDetail from "../../Components/repoDetail"; */
import { BrowserRouter as Router, Route } from "react-router-dom";

const styles = () => ({
  main: {
    background: "linear-gradient(to right, #444 50%, #efefef 50%);",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
    minHeight: "100vh",
    overflowY: "hidden"
  },
  container: {
    maxWidth: 1000,
    margin: "0 auto",
    width: "100%",
    minHeight: "100vh",
    display: "flex",
    // padding: "30px 0",
    boxSizing: "border-box"
  },
  content: {
    flex: 1,
    padding: "0 10px"
  },
  leftContent: {
    height: "100vh",
    overflowY: "scroll"
  },
  rightContent: {}
});

export default withStyles(styles)(
  class Repos extends React.Component {
    render() {
      const { classes, ...props } = this.props;

      return (
        <Router>
          <div className={classes.main}>
            <div className={classes.container}>
              <div className={`${classes.content} ${classes.leftContent}`}>
                <RepoList {...props} />
              </div>
              <div className={`${classes.content} ${classes.rightContent}`}>
                {
                  <Route
                    path={`${this.props.match.path}/:repo`}
                    component={props => {
                      console.log(
                        "repo por parametro",
                        props.match.params.repo
                      );
                      return <h1>Olá {props.match.params.repo}</h1>;
                    }}
                  />
                }
              </div>
            </div>
          </div>
        </Router>
      );
    }
  }
);
