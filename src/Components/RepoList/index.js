import React from "react";
import api from "../../services/api";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/styles";
import ListItem from "../ListItem";

const styles = {
  listTitle: {
    fontSize: "2rem",
    paddingLeft: 12,
    padding: 12,
    fontWeight: "lighter",
    position: "sticky",
    top: 0,
    background: "rgba(999,999,999,.6)",
    boxShadow: "-2px 2px 4px rgba(0,0,0,.4)"
  },
  link: {
    textDecoration: "none"
  }
};

export default withStyles(styles)(
  class RepoList extends React.Component {
    state = {
      repos: []
    };

    async componentDidMount() {
      const { user } = this.props.match.params;

      let res = await api.get(
        `/search/repositories?q=user:${user}&sort=stars&type=Repositories&order=desc`
      );

      this.setState({ repos: res.data.items });
      localStorage.setItem("repos", JSON.stringify(this.state.repos));
      console.log("repos", this.state.repos);
    }

    render() {
      const { classes } = this.props;
      const { repos } = this.state;
      const { url } = this.props.match;

      return (
        <React.Fragment>
          <div className={classes.listTitle}>Repositórios</div>
          {repos.map((e, i) => {
            return (
              <Link key={e.id} to={`${url}/${e.name}`} className={classes.link}>
                <ListItem name={e.name} stars={e.stargazers_count} />
              </Link>
            );
          })}
        </React.Fragment>
      );
    }
  }
);
