import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Repos from "./pages/Repos";
import Home from "./pages/Home";

const Routes = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/:user" component={Repos} />
      {/* <Route path="/:user/:repo" component={SignUp} /> */}
    </Switch>
  </Router>
);

export default Routes;
